package com.example.ui;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ejlchina.okhttps.HTTP;
import com.ejlchina.okhttps.HttpResult;
import com.ejlchina.okhttps.OkHttps;
import com.ejlchina.okhttps.WebSocket;
import com.example.R;
import com.example.okhttps.Tags;

public class MainActivity extends AppCompatActivity {

    WebSocket ws;
    Handler handler = new Handler();

    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        printThread();

//        ws = OkHttps.webSocket("ws://121.40.165.18:8800")
//                .setOnOpen((ws, res) -> {
//                    printThread();
//                    Log.i("HTTP-WS", "连接已打开：" + res);
//                    sendMsg();
//                })
//                .nextOnIO()
//                .setOnMessage((ws, msg) -> {
//                    printThread();
//                    Log.i("HTTP-WS", msg.toString());
//                })
//                .setOnException((ws, e) -> {
//                    printThread();
//                    Log.i("HTTP-WS", "发生异常");
//                    e.printStackTrace();
//                    this.ws = null;
//                })
//                .listen();

        OkHttps.async("/comm/provinces")
                .bind(this)
                .tag(Tags.TOKEN)
                .setOnResList(Province.class, list -> {
                    Log.i("OkHttps", list.toString());
                })
                .get();

    }

    static class Province {

        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Province{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    void sendMsg() {
        handler.postDelayed(() -> {
            if (ws != null) {
                ws.send("你好 " + i++);
                Log.i("HTTP-WS", "send 你好 " + (i - 1));
                sendMsg();
            }
        }, 3000);
    }

    void printThread() {
        Thread ct = Thread.currentThread();
        Log.i("HTTP", "Thread [id: " + ct.getId() + ", name: " + ct.getName() + "]");
    }

}
